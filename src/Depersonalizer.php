<?php
/**
 * Created by PhpStorm.
 * User: pbunaev
 * Date: 13.09.18
 * Time: 15:58
 */
declare(strict_types=1);
namespace Depersonalizer;

class Depersonalizer {
    const INSERT_TEMPLATE = 'INSERT INTO `%s` (';
    const DISPOSE_LINE = '';
    const IGNORE_LINE = 'VALUES';
    /**
     * @var Parameters
     */
    private $parameters;

    private $targetHandle;

    public function __construct(Parameters $parameters)
    {
        $this->parameters = $parameters;
    }
    private function readFileGenerator($file)
    {
        $fp = fopen($file, 'rb');
        while(($line = fgets($fp)) !== false) {
            yield rtrim($line, "\r\n");
        }
        fclose($fp);
    }

    private $flag_values_header  = false;
    private $flag_values_process = false;
    private $flag_ignore_table   = false;
    private $find_columns = [];
    private $use_table_name = '';

    /**
     * процессинг чтения и деперсонализации файла
     */
    public function process(): void
    {
        $this->targetHandle = \fopen($this->parameters->getTarget(),'w');
        foreach($this->readFileGenerator($this->parameters->getOriginal()) as $line)
        {
            \fwrite($this->targetHandle,$this->depersonalizedProcess($line).\PHP_EOL);
        }
        \fclose($this->targetHandle);
    }

    /**
     * Итерация выполняемая над 1 строкой
     * @param string $line
     * @return string
     */
    public function depersonalizedProcess(string $line): string
    {
        $this->defineMetaData($line);
        $this->defineValueMetaHeader($line);
        return $this->parseValueLine($line);
    }

    /**
     * Определение мета информации по строке
     * (обработка инструкции "INSERT" или "VALUES"
     * @param string $line
     */
    public function defineMetaData(string $line): void
    {
        if ($this->flag_ignore_table === false){
            foreach($this->parameters->getTables() as $table) {
                $start_insert_sql = \sprintf($this::INSERT_TEMPLATE,$table);
                if (strpos($line,$start_insert_sql) !== false ) {
                    $this->flag_ignore_table = true;
                    $this->flag_values_header = true;
                    $this->use_table_name = $table;
                    $columns            = explode(', ',str_replace([$start_insert_sql,'`', ')'],'',$line));
                    $this->find_columns       = array_intersect($columns, $this->parameters->getColumns($this->use_table_name));
                    break;
                }
            }
        }
    }

    /**
     * Определение мета информации инструкции "VALUES"
     * Начало списка значений или конец инструкции
     * @param string $line
     */
    public function defineValueMetaHeader(string $line): void
    {
        if ($this->flag_values_header && $line === 'VALUES') {
            $this->flag_values_process = true;
            return;
        }
    }

    /**
     * Парсим строку со значениями
     * @param string $line
     * @return string
     */
    public function parseValueLine(string $line): string
    {
        if ($this->flag_values_header && $this->flag_values_process && $line != $this::IGNORE_LINE) {
            if($line === $this::DISPOSE_LINE) {
                $this->disposeFlags();
                return $line;
            }
            return $this->depersonalizedLine($line);
        }
        return $line;
    }

    /**
     * Сбрасываем флаги мета информации
     */
    public function disposeFlags(): void
    {
        $this->find_columns = [];
        $this->flag_ignore_table = false;
        $this->flag_values_process = false;
        $this->flag_values_header = false;
    }

    /**
     * Возвращает деперсонализированную строку
     * @param string $line
     * @return string
     */
    public function depersonalizedLine(string $line): string
    {
        $personal_data   = explode(',', substr($line, 2, -2));
        $replace_columns = array_fill_keys(array_keys(($this->find_columns)), "''");
        $personal_data   = array_replace($personal_data, $replace_columns);
        return "\t(" . implode(', ', $personal_data) . ")" . substr($line, -1);
    }

}